SessionRPs = {}

-- Startup
local startingKills = GameData.Player.RvRStats.LifetimeKills
local startRP = 0
local startMinutesPlayed = GameData.Tome.Statistics.playedTime
local playerName = nil
local serverName = nil
local careerName = nil
local playerLevel = nil
local hoursPlayed = 0
local minutesPlayed = 0
local lastUpdate = {}
local Locale = LibStub("WAR-AceLocale-3.0"):GetLocale("SessionRPs", true)
local highScoreWindowVisible = false
local sortedHighScore = {}
local sessionTime = 000000
local lastTime = 000000

-- Sort Indexes
SessionRPs.SORT_ORDER_UP = 1
SessionRPs.SORT_ORDER_DOWN = 2
-- Total Lifetime
local updatedKills = GameData.Player.RvRStats.LifetimeKills
local totalRP = 0
local updatedMinutesPlayed = GameData.Tome.Statistics.playedTime

-- Current
local currentRP = GameData.Player.Renown.curRenownEarned
local currentRR = nil
local todaysDateServer = GetTodaysDate()
local todaysDay = 0
local currentMinutesPlayed = updatedMinutesPlayed - startMinutesPlayed
local sessionDB = 0
local sessionRenown = 0
local sessionKills = 0
local sessionDeath = 0
local careerLine = GameData.Player.career.line
local careerIconID = Icons.GetCareerIconIDFromCareerLine(tonumber(careerLine))
local highScoreBeaten = false
SessionRPs.debug = false

------
function NewSortData( param_label, param_varName )
    return { label=param_label, variable=param_varName }
end

SessionRPs.SORT_TYPE_NAME = 1

SessionRPs.sortData = {}
SessionRPs.sortData[1] = NewSortData( "Name",		"name" )
SessionRPs.sortData[2] = NewSortData( "Renown",		"points" )
SessionRPs.sortData[3] = NewSortData( "Kills",		"kills" )
SessionRPs.sortData[4] = NewSortData( "Deaths",		"deaths" )
SessionRPs.sortData[5] = NewSortData( "DBS",		"dbs" )
SessionRPs.sortData[6] = NewSortData( "Session",	"played" )
SessionRPs.sortData[7] = NewSortData( "Date",		"newdate" )

SessionRPs.display = { type=SessionRPs.SORT_TYPE_NAME,
                                  order=SessionRPs.SORT_ORDER_UP }
------

-- Accumulated Total Renownpoints per rank, i.e 10+80+230 etc
local rankRPs = 	{10,90,320,760,1490,2570,4070,6070,8630,11820,15710,20360,25850,32240,39590,47970,57450,68090,79950,93100,107600,123520,140910,159840,180370,
202560,226470,252170,279710,309150,340550,373970,409460,447090,486910,528970,573340,620070,669210,720820,774950,831660,891000,953020,1017780,1085320,1155700,1228970,
1305190,1384400,1466650,1552360,1641990,1736020,1834980,1939450,2050060,2167480,2292440,2425720,2568160,2720640,2884100,3059550,3248050,3450740,3668800,3903480,4156110,
4428080,4720850,5035950,5374980,5739630,6131650,6552880,7005230,7490690,8011340,8569350,}

function SessionRPs.Initialize()
   -- Register the slash command.  Now when the user types "/srp" the SessionRPs.Slash
   -- function will be actioned.
	--LibSlash.RegisterSlashCmd("srp", SessionRPs.Slash)
	LibSlash.RegisterSlashCmd("srp",function(msg) SessionRPs.Slash(msg) end)

	-- Create the window and then hide it.
	CreateWindow("SessionRPs", true)
	WindowSetShowing("SessionRPs",true)

	CreateWindow("NewHighScore", true)
	WindowSetShowing("NewHighScore",false)


	-- Register the window with the LayoutEditor so that it can be moved and resized via the
	-- Settings/Layout Editor screen. Note: You will need to have the SaveSettings="true" option
	-- set in the <Window> XML tag if you want these changes to persist beyond game closure.
	LayoutEditor.RegisterWindow ( "SessionRPs"             -- Window Name
							   , L"SessionRPs"            -- Window Display Name
							   , L"SessionRPs"				 -- Window Description
							   , false                       -- Allow Size Width
							   , false                       -- Allow Size Height
							   , true                        -- Allow Hiding
							   , nil                         -- Allowable Anchor List
							   )
	sessionBackup = false
	SessionRPs.PrintD("SessionRPs.Initialize")
 	RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "SessionRPs.Start")
	RegisterEventHandler(SystemData.Events.LOADING_END, "SessionRPs.Start" )
end



function SessionRPs.Start()
SessionRPs.PrintD("SessionRPs.Start Start")
   -- Create settings file if it doesn't exist
	if (not SessionRPs.HighScore) then
	  SessionRPs.HighScore = {}
		for i = 1, 12 do
			local newEntry =
			{
				name = "N/A",
				newdate = "20100101010101",
				points = 0,
				dbs = 0,
				kills = 0,
				played = 0,
				careerline = 0,
				careername = "N/A",
				deaths = 0,
				server = "N/A",
				level = 0,
			}
			table.insert(SessionRPs.HighScore, newEntry);
		end
	end
	SessionRPs.ListIndex = {}
	for i = 1, 10 do
	SessionRPs.ListIndex[i] = i
	end
	if (not SessionRPs.DeleteEntry) then
		SessionRPs.DeleteEntry = "0"
	end
	SessionRPs.Version = "1.12"
	UnregisterEventHandler(SystemData.Events.LOADING_END, "SessionRPs.Start" )
	RegisterEventHandler( SystemData.Events.PLAYER_RENOWN_UPDATED, "SessionRPs.Update" )
	RegisterEventHandler(SystemData.Events.PLAYER_RVR_STATS_UPDATED, "SessionRPs.Kill")
	RegisterEventHandler(SystemData.Events.PLAYER_DEATH, "SessionRPs.Death")
	SessionRPs.VersionFix()
	SessionRPs.ResetStats()
	SessionRPs.Update()
	SessionRPs.PrintChat(Locale["Init"])
	SessionRPs.PrintD("SessionRPs.Start End")
end

function SessionRPs.VersionFix()
	if (not SessionRPs.HighScore[12]) then
		local newEntry = SessionRPs.HighScore[11]
		table.insert(SessionRPs.HighScore, newEntry);
	end
	-- Convert old damte to new date format
	if SessionRPs.HighScore[1].date then
		SessionRPs.PrintChat(Locale["NewDate"])
		local newDay = 0
		local newYear = 0
		local newMonth = 0
			for i = 1, 11 do
				newYear = SessionRPs.HighScore[i].date.todaysYear
				newMonth = SessionRPs.HighScore[i].date.todaysMonth
				newDay = SessionRPs.HighScore[i].date.todaysDay
				SessionRPs.HighScore[i].newdate = newYear .. TwoDigit(newMonth) .. TwoDigit(newDay) .. "010101"
				SessionRPs.HighScore[i].date = nil
			end
	end
	-- Convert number date to string date with time
	for i = 1, 12 do
		if string.len(SessionRPs.HighScore[i].newdate) == 8 then
			SessionRPs.HighScore[i].newdate = SessionRPs.HighScore[i].newdate .. "010101"
		end
	end
end

function SessionRPs.FetchStats()
	playerLevel = GameData.Player.level
	playerName = WStringToString(SessionRPs.ClearNameString(GameData.Player.name))
	serverName = WStringToString(GameData.Account.ServerName)
	careerName = GameData.Player.career.name
	currentRP = GameData.Player.Renown.curRenownEarned
	currentRR = GameData.Player.Renown.curRank
	updatedKills = GameData.Player.RvRStats.LifetimeKills
	updatedDB = GameData.Player.RvRStats.LifetimeDeathBlows
	updatedMinutesPlayed = GameData.Tome.Statistics.playedTime
	careerLine = GameData.Player.career.line
	careerIconID = Icons.GetCareerIconIDFromCareerLine(careerLine)
	updatedDeath = GameData.Player.RvRStats.LifetimeDeaths
	todaysDateServer = GetTodaysDate()
	if TextLogGetNumEntries("Chat") > 0 then
		local lastChatEntry = TextLogGetNumEntries("Chat") - 1
		local time, _, _ = tostring(TextLogGetEntry("Chat", lastChatEntry))
		local _, _, hour, minute, second = string.find(time, "(%d+):(%d+):(%d+)")
		sessionTime = hour .. minute .. second
	end
	todaysDate = todaysDateServer.todaysYear .. TwoDigit(todaysDateServer.todaysMonth) .. TwoDigit(todaysDateServer.todaysDay) .. sessionTime
	SessionRPs.PrintD(sessionTime)
end

function SessionRPs.CloseWindow()
   --When rightclicking window, close window.
   WindowSetShowing("SessionRPs",false)
end

function SessionRPs.Slash(_msg)
--When the user types "/srp", show the SessionRPs window
	local msg = string.lower(_msg)
	if msg == "" then
		WindowSetShowing("SessionRPs",true)
	elseif msg == "reset" then
		SessionRPs.ResetSave()
	elseif msg == "save" then
		SessionRPs.ResetSave()
	elseif msg == "clearhighscores" then
		SessionRPs.ClearHighScores()
	elseif msg == "help" then
		SessionRPs.ShowHelp()
	else
	SessionRPs.PrintChat(Locale["Unknown"] .. _msg)
	end
end


function SessionRPs.ShowHelp()
	SessionRPs.PrintChat(Locale["SRP"])
	SessionRPs.PrintChat(Locale["RightClick"])
	SessionRPs.PrintChat(Locale["MiddleClick"])
	SessionRPs.PrintChat(Locale["SaveReset"])
	SessionRPs.PrintChat(Locale["ClearHS"])
end

function SessionRPs.HighScoreOnMouseOver()
    -- Create the tooltip
	local _windowID = WindowGetId(SystemData.ActiveWindow.name)
	local _sortIndex = SessionRPs.ListIndex[_windowID]
	local _windowName = SystemData.ActiveWindow.name
	WindowSetShowing( _windowName .."BG", false )
	WindowSetShowing( _windowName .."HighLight", true )
	Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil )

	Tooltips.SetTooltipText( 1, 1, Locale["Character"] .. towstring(SessionRPs.HighScore[_sortIndex].name))
	Tooltips.SetTooltipText( 2, 1, Locale["Class"] .. towstring(SessionRPs.HighScore[_sortIndex].careername))
	Tooltips.SetTooltipText( 3, 1, Locale["Level"] .. towstring(SessionRPs.HighScore[_sortIndex].level))
	Tooltips.SetTooltipText( 4, 1, Locale["Server"] .. towstring(SessionRPs.HighScore[_sortIndex].server))

	Tooltips.Finalize()
	Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_LEFT)

end

function SessionRPs.HighScoreOnMouseOverEnd()
	local _windowName = SystemData.ActiveWindow.name
	WindowSetShowing( _windowName .."BG", true )
	WindowSetShowing( _windowName .."HighLight", false )
end
-- Reset starting stats
function SessionRPs.ResetStats()
SessionRPs.PrintD("SessionRPs.ResetStats Start")
	SessionRPs.FetchStats()
	if currentRR == nil then
		SessionRPs.FetchStats()
	elseif currentRR > 0 then
		totalRP = rankRPs[currentRR]
		totalRP = totalRP + currentRP
		startRP = totalRP
	elseif currentRR == 0 then
		totalRP = 0
		totalRP = totalRP + currentRP
		startRP = totalRP
	end
	startingKills = GameData.Player.RvRStats.LifetimeKills
	startMinutesPlayed = GameData.Tome.Statistics.playedTime
	sessionDB = 0
	sessionRenown = 0
	sessionKills = 0
	SessionRPs.checkTime()
SessionRPs.PrintD("SessionRPs.ResetStats End")
end

-- Update session stats
function SessionRPs.Update()
SessionRPs.PrintD("SessionRPs.Update Start")
	SessionRPs.FetchStats()
	sessionKills = updatedKills - startingKills
	currentMinutesPlayed = updatedMinutesPlayed - startMinutesPlayed
		if currentRR == nil then
			SessionRPs.ResetStats()
		elseif currentRR > 0 then
			totalRP = rankRPs[currentRR]
			totalRP = totalRP + currentRP
		elseif currentRR == 0 then
			totalRP = 0
			totalRP = totalRP + currentRP
		end
	sessionRenown = totalRP - startRP
	totalRP = 0
	ListBoxSetDisplayOrder("NewHighScoreList", SessionRPs.ListIndex)
	SessionRPs.checkHighScore(sessionRenown, sessionDB)
	SessionRPs.CreateWindow()
	SessionRPs.PrintD(FixDate(SessionRPs.HighScore[1].newdate))
SessionRPs.PrintD("SessionRPs.Update End")
end

function SessionRPs.PopulateList()
    if ( NewHighScoreList.PopulatorIndices == nil ) then return end  -- Avoids messages in the error log

    for row, v in ipairs(NewHighScoreList.PopulatorIndices) do
        local rowFrame   = "NewHighScoreListRow".. row

        LabelSetText(rowFrame .. "Pos", towstring( row .. "."))
        LabelSetText(rowFrame .. "Icon", SessionRPs.GetCareerIcon(SessionRPs.HighScore[v].careerline))
        LabelSetText(rowFrame .. "Name", towstring(SessionRPs.HighScore[v].name))
        LabelSetText(rowFrame .. "Session", PlayedTime(SessionRPs.HighScore[v].played))
        LabelSetText(rowFrame .. "Date", FixDate(SessionRPs.HighScore[v].newdate))


		if math.mod(row, 2) == 0 then
			WindowSetTintColor(rowFrame .. "BG", 0, 0, 0)
		end

        WindowSetShowing(rowFrame .. "HighLight", false)

		LabelSetText("NewHighScoreTitleBarText", Locale["Highscores"])
		LabelSetText("NewHighScoreNameLabel", Locale["Name"])
		LabelSetText("NewHighScoreSessionLabel", Locale["HSSession"])
		LabelSetText("NewHighScoreRenownLabel", Locale["Renown"])
		LabelSetText("NewHighScoreKillsLabel", Locale["HSKills"])
		LabelSetText("NewHighScoreDeathsLabel", Locale["Deaths"])
		LabelSetText("NewHighScoreDBSLabel", Locale["dbs"])
		LabelSetText("NewHighScoreDateLabel", Locale["Date"])

    end
end


function SessionRPs.checkHighScore(_sessionRenown, _sessionDB)
if currentRR == 80 then
	if not highScoreBeaten and _sessionDB > SessionRPs.HighScore[1].dbs then
		PlaySound(GameData.Sound.ADVANCE_RANK)
		AlertTextWindow.AddLine(SystemData.AlertText.Types.MOVEMENT_RVR, Locale["BeatHigh"])
		highScoreBeaten = true
	end
else
	if not highScoreBeaten and _sessionRenown > SessionRPs.HighScore[1].points then
		PlaySound(GameData.Sound.ADVANCE_RANK)
		AlertTextWindow.AddLine(SystemData.AlertText.Types.MOVEMENT_RVR, Locale["BeatHigh"])
		highScoreBeaten = true
	end
end

end

-- Check if Last Session Time is less than 5 minutes since Current Session time, if so ask to merge.
function SessionRPs.checkTime()
	if SessionRPs.HighScore[12].newdate then
		lastTime = string.sub(SessionRPs.HighScore[12].newdate, -6, -1)
		local text1 = "Notice"
		local text2 = "MergeNotice"
		local _lastChar = SessionRPs.HighScore[12].name
		local _lastHour = string.sub(lastTime, 1, 2)
		local _lastMin = string.sub(lastTime, 3, 4)
		local _sessionHour = string.sub(sessionTime, 1, 2)
		local _sessionMin = string.sub(sessionTime, 3, 4)
		local _lastServer = SessionRPs.HighScore[12].server
		local _lastDate = string.sub(SessionRPs.HighScore[12].newdate, 1, 8)
		local _sessionDate = string.sub(todaysDate, 1, 8)
		SessionRPs.PrintD( _lastHour )
		SessionRPs.PrintD( _sessionHour)
		SessionRPs.PrintD( _lastDate )
		SessionRPs.PrintD( _sessionDate)
			if _lastChar == playerName and _lastServer == serverName then
				if _lastHour == "23" and _sessionHour == "00" then
					_sessionHour = 24
					_sessionDate = _sessionDate + 1
				end
				local _lastMinutes = (_lastHour * 60) + _lastMin
				local _sessionMinutes = (_sessionHour * 60) + _sessionMin
				if ((_sessionMinutes - _lastMinutes) <= 5 ) and _lastDate == _sessionDate then
					SessionRPs.WarningWindow(text1, text2)
				end
			end
	end
end


function SessionRPs.CreateWindow()
SessionRPs.PrintD("SessionRPs.CreateWindow Start")
	LabelSetText("SessionRPsSessionDeaths", towstring(sessionDeath))
	LabelSetText("SessionRPsSessionDB", towstring(sessionDB))
	LabelSetText("SessionRPsSessionRP", towstring(sessionRenown))
	LabelSetText("SessionRPsLastDeaths", towstring(SessionRPs.HighScore[12].deaths))
	LabelSetText("SessionRPsLastDB", towstring(SessionRPs.HighScore[12].dbs))
	LabelSetText("SessionRPsLastRP", towstring(SessionRPs.HighScore[12].points))
	LabelSetText("SessionRPsHighDeaths", towstring(SessionRPs.HighScore[1].deaths))
	LabelSetText("SessionRPsHighDB", towstring(SessionRPs.HighScore[1].dbs))
	LabelSetText("SessionRPsHighRP", towstring(SessionRPs.HighScore[1].points))
	LabelSetText("SessionRPsLastLabel", Locale["Last"])
	LabelSetText("SessionRPsCurLabel", Locale["Session"])
	LabelSetText("SessionRPsHighLabel", Locale["High"])
	LabelSetText("SessionRPsDeathsLabel", Locale["Deaths"])
	LabelSetText("SessionRPsDBLabel", Locale["dbs"])
	LabelSetText("SessionRPsRPLabel", Locale["rps"])
SessionRPs.PrintD("SessionRPs.CreateWindow End")

end

-- Convert single digit to two digits, i.e 1 to 01
function TwoDigit(value)
	if (value < 10) then value = "0"..value end
	return (value)
end

-- Convert date to string.
function FixDate(savedDate)
	local saveDay = towstring(string.sub(savedDate, 7, 8))
	local saveMonth	 = towstring(string.sub(savedDate, 5, 6))
	local saveYear = towstring(string.sub(savedDate, 1, 4))
	return (saveDay .. L"/" .. saveMonth .. L"/" .. saveYear)
end

-- Convert minutesPlayed to hours and minutes
function PlayedTime( _currentMinutesPlayed )
hoursPlayed = math.floor(_currentMinutesPlayed / 60)
minutesPlayed = _currentMinutesPlayed
	if ( _currentMinutesPlayed >= 60 ) then
		hoursPlayed = math.floor(_currentMinutesPlayed / 60)
		minutesPlayed = _currentMinutesPlayed - (hoursPlayed * 60)
		return(towstring( hoursPlayed .. L"h " .. minutesPlayed .. L"m" ))
	elseif ( _currentMinutesPlayed < 60 ) then
		minutesPlayed = _currentMinutesPlayed
		return(towstring( minutesPlayed .. L" min" ))
	end

end

function SessionRPs.ToggleHighScore()
	if not highScoreWindowVisible then
		WindowSetShowing("NewHighScore",true)
		highScoreWindowVisible = true
	elseif highScoreWindowVisible then
		WindowSetShowing("NewHighScore",false)
		highScoreWindowVisible = false
	end
end

----- Reset Functions -----
function SessionRPs.ResetSave()
	local text1 = "Warning"
	local text2 = "ResetWarning"
	SessionRPs.WarningWindow(text1, text2)
end

function SessionRPs.ClearHighScores()
	local text1 = "Warning"
	local text2 = "ClearWarning"
	SessionRPs.WarningWindow(text1, text2)
end

function SessionRPs.WarningWindow(_text1, _text2)
	local windowname = "SessionRPs" .. _text2 .. "Window"
	CreateWindowFromTemplate(windowname, "SessionRPsWarningTemplate", "Root")
	LabelSetText(windowname.."Text", Locale[_text2])
	LabelSetText(windowname.."TitleBarText", Locale[_text1])
	LabelSetTextColor(windowname.."Text", 240, 40, 40)
	CreateWindowFromTemplate(windowname.."Button1", "SessionRPsButtonTemplate", windowname)
	CreateWindowFromTemplate(windowname.."Button2", "SessionRPsButtonTemplate", windowname)
	WindowClearAnchors(windowname.."Button1")
	WindowAddAnchor(windowname.."Button1", "bottomleft", windowname, "bottomleft", 30, -10)
	WindowAddAnchor(windowname.."Button2", "bottomright", windowname, "bottomright", -30, -10)
	WindowSetDimensions(windowname.."Button2", 100, 40)
	WindowSetDimensions(windowname.."Button1", 100, 40)
	ButtonSetText(windowname.."Button1", Locale["Yes"])
	ButtonSetText(windowname.."Button2", Locale["No"])
end

function SessionRPs.WarningWindowButton()
    local mouseWindowname = SystemData.MouseOverWindow.name
	local windowname = string.gsub(mouseWindowname, "Button%d+", "")
	local buttonnumber = tonumber(string.sub(mouseWindowname, -1))
	if buttonnumber == 1 and windowname == "SessionRPsResetWarningWindow" then
		SessionRPs.PrintD("Pressed Button 1, Reset")
		DestroyWindow(windowname)
		SessionRPs.PrintChat("Saving Highscore")
		SessionRPs.Update()
		InterfaceCore.ReloadUI()
	elseif buttonnumber == 1 and windowname == "SessionRPsClearWarningWindow" then
		SessionRPs.PrintD("Pressed Button 1, Clear")
		DestroyWindow(windowname)
		SessionRPs.HighScore = nil
		InterfaceCore.ReloadUI()
	elseif buttonnumber == 1 and windowname =="SessionRPsMergeNoticeWindow" then
		SessionRPs.PrintD("Pressed Button 1, Merge")
		SessionRPs.MergeLast()
		DestroyWindow(windowname)
	elseif buttonnumber == 2 then
		SessionRPs.PrintD("Pressed 2, Aborting")
		DestroyWindow(windowname)
	end
end

function SessionRPs.MergeLast()
SessionRPs.PrintD("SessionRPs.MergeLast Start")
	startRP = startRP - SessionRPs.HighScore[12].points
	sessionDeath = SessionRPs.HighScore[12].deaths
	startingKills = startingKills - SessionRPs.HighScore[12].kills
	sessionDB = SessionRPs.HighScore[12].dbs
	startMinutesPlayed = startMinutesPlayed - SessionRPs.HighScore[12].played
	SessionRPs.HighScore[12].points = 0
	SessionRPs.HighScore[12].deaths = 0
	SessionRPs.HighScore[12].dbs = 0
	SessionRPs.HighScore[12].kills = 0
	SessionRPs.HighScore[12].played = 0

	-- Prepare last highscore entry for removal
	SessionRPs.DeleteEntry = SessionRPs.HighScore[12].newdate

	SessionRPs.Update()
SessionRPs.PrintD("SessionRPs.MergeLast End")
end

function SessionRPs.DeleteIfMerge()
	local i = 1
		while i <= 10 do
			if SessionRPs.HighScore[i].newdate == SessionRPs.DeleteEntry then
				local newEntry =
				{
					name = newName,
					newdate = newDate,
					points = newPoints,
					dbs = newDB,
					kills = newKills,
					played = newPlayed,
					careerline = newCareer,
					careername = newCareerName,
					deaths = newDeaths,
					server = newServer,
					level = newLevel,
				}
				table.remove(SessionRPs.HighScore, i)
				table.insert(SessionRPs.HighScore, 11, newEntry)
				break
			end
			i = i + 1
		end
end
-------------------------

function SessionRPs.GetSortType()

    local type = WindowGetId( SystemData.ActiveWindow.name )
	local _windowName = SystemData.ActiveWindow.name
	SessionRPs.PrintD(type)
    -- If we are already using this sort type, toggle the order.
    if( type == SessionRPs.display.type ) then
        if( SessionRPs.display.order == SessionRPs.SORT_ORDER_UP ) then
            SessionRPs.display.order = SessionRPs.SORT_ORDER_DOWN
        else
            SessionRPs.display.order = SessionRPs.SORT_ORDER_UP
        end

    -- Otherwise change the type and use the up order.
    else
        SessionRPs.display.type = type
        SessionRPs.display.order = SessionRPs.SORT_ORDER_UP
    end

	SessionRPs.ResetSortColor()
	LabelSetTextColor(_windowName, 255, 255, 255)
	SessionRPs.UpdateHighScoreList()

end

function SessionRPs.UpdateHighScoreList()

    -- Filter, Sort, and Update
    SessionRPs.SortHighScoreList()
    ListBoxSetDisplayOrder( "NewHighScoreList", SessionRPs.ListIndex )

end

function SessionRPs.SortHighScoreList()

    local type = ScenarioSummaryWindow.display.type
    local order = ScenarioSummaryWindow.display.order

    --DEBUG(L" Sorting Players: type="..type..L" order="..order )
    table.sort( SessionRPs.ListIndex, SessionRPs.CompareHighScore )

end


function SessionRPs.CompareHighScore(index1, index2)
    if( index2 == nil ) then
        --DEBUG(L" ComparePlayers( "..index1..L", nil )" )
        return false
    end

    --DEBUG(L" ComparePlayers( "..index1..L", "..index2..L" )" )

    local highscore1 = SessionRPs.HighScore[index1]
    local highscore2 = SessionRPs.HighScore[index2]

    local sortType = SessionRPs.display.type
    local order = SessionRPs.display.order

    -- Sorting By Name
    if( sortType == SessionRPs.SORT_TYPE_NAME ) then
        if( order == SessionRPs.SORT_ORDER_UP ) then
            return ( WStringsCompare(highscore1.name, highscore2.name) < 0 )
        else
            return ( WStringsCompare(highscore1.name, highscore2.name) > 0 )
        end
    end

    -- Sorting By A Numerical Value - When tied, sort by name
    local key = SessionRPs.sortData[sortType].variable
    --DEBUG(L" key = "..StringToWString(key))
    --DEBUG(L" value1="..highscore1[key]..L", value2="..highscore2[key] )

    local dataType = type( highscore1[key] )
    if( order == SessionRPs.SORT_ORDER_UP ) then

        if( highscore1[key] == highscore2[key] ) then
            return ( WStringsCompare(highscore1.name, highscore2.name) < 0 )
        else
            if( dataType == "wstring" ) then
                return ( WStringsCompare(highscore1[key], highscore2[key]) > 0 )
            elseif( dataType == "number" ) then
                return ( highscore1[key] > highscore2[key] )
            elseif( dataType == "string" ) then
                return ( highscore1[key] > highscore2[key] )
            end
        end
    else
        if( highscore1[key] == highscore2[key] ) then
            return ( WStringsCompare(highscore1.name, highscore2.name) < 0 )
        else
            if( dataType == "wstring" ) then
                return ( WStringsCompare(highscore1[key], highscore2[key]) < 0 )
            elseif( dataType == "number" ) then
                return ( highscore1[key] < highscore2[key] )
            elseif( dataType == "string" ) then
                return ( highscore1[key] < highscore2[key] )
            end
        end
    end

end

function SessionRPs.ResetSortColor()
	LabelSetTextColor("NewHighScoreRenownLabel", 255, 204, 102)
	LabelSetTextColor("NewHighScoreDeathsLabel", 255, 204, 102)
	LabelSetTextColor("NewHighScoreKillsLabel", 255, 204, 102)
	LabelSetTextColor("NewHighScoreSessionLabel", 255, 204, 102)
	LabelSetTextColor("NewHighScoreDBSLabel", 255, 204, 102)
	LabelSetTextColor("NewHighScoreNameLabel", 255, 204, 102)
	LabelSetTextColor("NewHighScoreDateLabel", 255, 204, 102)
end


-- Convert careerline to iconid
function SessionRPs.GetCareerIcon(_careerLine)
	if _careerLine == 0 then
		careerIconID = 43
		return (L"<icon".. careerIconID .. L">")
	else
		careerIconID = Icons.GetCareerIconIDFromCareerLine(_careerLine)
		return (L"<icon".. careerIconID .. L">")
	end
end

function SessionRPs.CloseHSWindow()
   --When the button is clicked, hide the window.
	WindowSetShowing("NewHighScore",false)
	highScoreWindowVisible = false
end

-- Add new highscore entry
function SessionRPs.NewEntry(newName, newPoints, newDate, newDB, newKills, newPlayed, newCareer, newDeaths, newServer, newCareerName, newLevel)
	local i = 1
		while i <= 10 do
			if currentRR == 80 then
				if newDB >= SessionRPs.HighScore[i].dbs and newDB ~= 0 then
					local newEntry =
					{
						name = newName,
						newdate = newDate,
						points = newPoints,
						dbs = newDB,
						kills = newKills,
						played = newPlayed,
						careerline = newCareer,
						careername = newCareerName,
						deaths = newDeaths,
						server = newServer,
						level = newLevel,
					}
					table.remove(SessionRPs.HighScore, 11)
					table.insert(SessionRPs.HighScore, i, newEntry)
					break
				end
			else
				if newPoints >= SessionRPs.HighScore[i].points and newPoints ~= 0 then
					local newEntry =
					{
						name = newName,
						newdate = newDate,
						points = newPoints,
						dbs = newDB,
						kills = newKills,
						played = newPlayed,
						careerline = newCareer,
						careername = newCareerName,
						deaths = newDeaths,
						server = newServer,
						level = newLevel,
					}
					table.remove(SessionRPs.HighScore, 11)
					table.insert(SessionRPs.HighScore, i, newEntry)
					break
				end
			end
			i = i + 1
		end
end


function SessionRPs.LastSession(lastName, lastPoints, lastDate, lastDB, lastKills, lastPlayed, lastCareer, lastDeath, lastServer, lastCareerName, lastLevel)
	if lastPoints >= 1 then
		local lastEntry =
			{
				name = lastName,
				newdate = lastDate,
				points = lastPoints,
				dbs = lastDB,
				kills = lastKills,
				played = lastPlayed,
				careerline = lastCareer,
				careername = lastCareerName,
				deaths = lastDeath,
				server = lastServer,
				level = lastLevel,
			}
			table.remove(SessionRPs.HighScore, 12)

			table.insert(SessionRPs.HighScore, lastEntry)
	end
end

-- Combat log parse functions etc taken from DamazKron and KD_Ratio --------------------------------------------------------------------

-- Remove male / female notation from name string.
function SessionRPs.ClearNameString(name)
	if type(name) == "wstring" and wstring.match(name, L"(.+)(%^)(.+)") then
		name = wstring.match(name, L"(.+)(%^)(.+)")
	elseif type(name) == "string" and string.match(name, "(.+)(%^)(.+)") then
		name = string.match(name, "(.+)(%^)(.+)")
	end
	return name
end

-- Update death blow and death
local function AddValue(val_type)
	if val_type == "death" then
		sessionDeath = sessionDeath + 1
	elseif val_type == "kill" then
		sessionDB = sessionDB + 1
	end
	SessionRPs.Update()
	SessionRPs.CreateWindow()
end

-- Check last killer name, if other than yourself add to death value
function SessionRPs.Death()
	if GameData.DeathRespawnData.Locations[1].name == L"" then return end -- Thanks to Book of Grudges
	local killerName = WStringToString(SessionRPs.ClearNameString(GameData.Player.killerName))
	if killerName == playerName then return end
	AddValue("death")
end

-- Parse combat log for your last kill, update death blow value
function SessionRPs.Kill()
	--Taken from BookOfGrudges/KD_Ratio and modified
	if TextLogGetNumEntries("Combat") > 0 then
		local lastIndex = TextLogGetNumEntries("Combat") - 1
		local contentLines = {}
--		local lastRenow = ""
		for index = lastIndex,(lastIndex - 20),-1 do
			if index < 0 then break end

			local time, _, text = TextLogGetEntry("Combat", index)
			text = WStringToString(text)

--			lastRenow = string.match(text, Locale["MatchRenown"]) or lastRenow
--			if lastRenow == "" then return end

			local loserName, winnerName = string.match(text, Locale["MatchKill"])

			if loserName and winnerName and loserName ~= winnerName then

				if lastUpdate.time == time and lastUpdate.loser == loserName and lastUpdate.winner == winnerName then break end

				if winnerName == playerName then
					lastUpdate = {time = time, loser = loserName, winner = winnerName}
					AddValue("kill")
				end
			end
		end
	end
end
-------------------------------------------------------------------------------------------------------------------------------------

-- Save Highscore list on shutdown
function SessionRPs.SaveSettings()
	SessionRPs.DeleteIfMerge()
	SessionRPs.NewEntry(playerName, sessionRenown, todaysDate, sessionDB, sessionKills, currentMinutesPlayed, careerLine, sessionDeath, serverName, careerName, playerLevel)
	SessionRPs.LastSession(playerName, sessionRenown, todaysDate, sessionDB, sessionKills, currentMinutesPlayed, careerLine, sessionDeath, serverName, careerName, playerLevel)
end

-- Debug print
function SessionRPs.PrintD( text )
	if(SessionRPs.debug) then TextLogAddEntry("Chat",65 , L"SRP DEBUG : ".. towstring(text)) end
end

function SessionRPs.PrintChat( text )
	TextLogAddEntry("Chat",65 , L"Session RP's : ".. towstring(text))
end

function SessionRPs.Shutdown()
	SessionRPs.SaveSettings()
 	UnregisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "SessionRPs.Start")
	UnregisterEventHandler( SystemData.Events.PLAYER_RENOWN_UPDATED, "SessionRPs.Update" )
	UnregisterEventHandler(SystemData.Events.PLAYER_RVR_STATS_UPDATED, "SessionRPs.Kill")
	UnregisterEventHandler(SystemData.Events.PLAYER_DEATH, "SessionRPs.Death")
	LibSlash.UnregisterSlashCmd( "srp" )
end
