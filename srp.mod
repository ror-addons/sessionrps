<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="SessionRPs" version="1.12" date="04/16/2010" >
        <Author name="Moewen" email="mistah_evil@hotmail.com" />
        <Description text="Records session renown points, deathblow, deaths etc, and save them in a global highscore list" />
        <VersionSettings gameVersion="1.3.4" windowsVersion="1.0" savedVariablesVersion="1.0"/>
        <Dependencies>
            <Dependency name="EASystem_WindowUtils" />
            <Dependency name="LibSlash" />
			<Dependency name="EA_TomeOfKnowledge" />
        </Dependencies>
        <Files>
			<File name="lib/LibStub.lua" />
			<File name="lib/AceLocale-3.0.lua" />

			<File name="lang/enUS.lua" />
			<File name="lang/frFR.lua" />
			<File name="lang/deDE.lua" />
			<File name="lang/esES.lua" />
			<File name="lang/itIT.lua" />

            <File name="srp.lua" />
            <File name="srp.xml" />
        </Files>
		<OnShutdown>
			<CallFunction name="SessionRPs.Shutdown" />
		</OnShutdown>
		<SavedVariables>
            <SavedVariable name="SessionRPs.HighScore" global="true" />
            <SavedVariable name="SessionRPs.Version" global="true" />
            <SavedVariable name="SessionRPs.DeleteEntry" global="true" />
        </SavedVariables>
        <OnInitialize>
            <CallFunction name="SessionRPs.Initialize" />
        </OnInitialize>
		<WARInfo>
			<Categories>
				<Category name="RVR" />
			</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="WITCH_ELF" />
				<Career name="DISCIPLE" />
				<Career name="SORCERER" />
				<Career name="IRON_BREAKER" />
				<Career name="SLAYER" />
				<Career name="RUNE_PRIEST" />
				<Career name="ENGINEER" />
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
				<Career name="WITCH_HUNTER" />
				<Career name="KNIGHT" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="CHOSEN" />
				<Career name= "MARAUDER" />
				<Career name="ZEALOT" />
				<Career name="MAGUS" />
				<Career name="SWORDMASTER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="WHITE_LION" />
				<Career name="ARCHMAGE" />
			</Careers>
		</WARInfo>
    </UiMod>
</ModuleFile>
